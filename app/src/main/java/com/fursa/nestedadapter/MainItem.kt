package com.fursa.nestedadapter

data class MainItem(
    val title: String,
    val items: List<NestedItem>
)

data class NestedItem(
    val title: String,
    val plan: String,
    val fact: String,
    val status: String,
    val percent: String
)