package com.fursa.nestedadapter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_main.*
import kotlinx.android.synthetic.main.item_main.recyclerViewMain

class MainActivity : AppCompatActivity() {
  private val mainAdapter = MainAdapter()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    val linearLayoutManager = LinearLayoutManager(applicationContext)


    mainAdapter.update(Data.mainItems)

    linearLayoutManager.apply {
      isSmoothScrollbarEnabled = false
      recycleChildrenOnDetach = false
      initialPrefetchItemCount = 20
    }

    recyclerView.apply {
      layoutManager = linearLayoutManager
      adapter = mainAdapter
    }
  }
}