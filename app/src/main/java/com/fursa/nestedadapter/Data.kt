package com.fursa.nestedadapter

object Data {
    private var childItems = mutableListOf<NestedItem>()
    var mainItems = mutableListOf<MainItem>()

    init {
        for (i in 0..800) {
            childItems.add(NestedItem("Nested item title $i", "10", "5", "in progress", "50"))
        }

        for (i in 0..10) {
            mainItems.add(MainItem("Title $i", childItems))
        }
    }


}