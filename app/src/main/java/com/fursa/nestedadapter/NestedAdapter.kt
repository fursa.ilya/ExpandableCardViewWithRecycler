package com.fursa.nestedadapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_main.view.*
import kotlinx.android.synthetic.main.item_nested.view.*

class NestedAdapter() : RecyclerView.Adapter<NestedAdapter.NestedItemViewHolder>() {
    private var items: MutableList<NestedItem> = mutableListOf()

    fun setItems(items: MutableList<NestedItem>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class NestedItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: NestedItem) = with(itemView) {
            txtTaskName.text = item.title
            txtPlan.text = item.plan
            txtFact.text = item.fact
            txtWorkStatus.text = item.status
            txtProgressPercent.text = item.percent
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NestedItemViewHolder {
        val nestedItemView = LayoutInflater.from(parent.context).inflate(R.layout.item_nested, parent, false)
        return NestedItemViewHolder(nestedItemView)
    }

    override fun onBindViewHolder(holder: NestedItemViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

}