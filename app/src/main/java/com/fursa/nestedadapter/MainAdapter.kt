package com.fursa.nestedadapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_main.view.*

class MainAdapter(): RecyclerView.Adapter<MainAdapter.MainItemViewHolder>() {
    private val items: MutableList<MainItem> = mutableListOf()

    fun update(newList: List<MainItem>) {
        val startPosition = items.size
        if (startPosition == 0) {
            items.addAll(newList)
            notifyDataSetChanged()
        } else {
            items.addAll(newList)
            notifyItemRangeInserted(startPosition, newList.size)
        }
    }

    inner class MainItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val nestedRecyclerView: RecyclerView = itemView.recyclerViewMain
        private val button: Button = itemView.button
        private val title: TextView = itemView.textViewMainHeader
        private val nestedAdapter = NestedAdapter()

        init {
            LinearLayoutManager(
                itemView.context,
                LinearLayoutManager.VERTICAL,
                false
            ).apply {
                isSmoothScrollbarEnabled = false
                recycleChildrenOnDetach = false
                initialPrefetchItemCount = 2
            }.also {
                nestedRecyclerView.apply {
                    layoutManager = it
                    setHasFixedSize(true)
                    adapter = nestedAdapter
                    visibility = View.GONE //Todo Listener не реагирует на прокрутку
                }.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                        super.onScrollStateChanged(recyclerView, newState)
                        Log.d("RecyclerView", "onScrollStateChanged")
                    }

                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)
                        Log.d("RecyclerView", "onScrolled")
                    }
                })
            }

            button.setOnClickListener {
                if(nestedRecyclerView.visibility == View.VISIBLE) {
                    nestedRecyclerView.visibility = View.GONE
                } else {
                    nestedRecyclerView.visibility = View.VISIBLE
                }
            }
        }

        fun bind(pos: Int) {
            val item = items[pos]
            title.text = item.title
            nestedAdapter.setItems(item.items.toMutableList())
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_main, parent, false)
        return MainItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: MainItemViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
       return items.count()
    }



}